import core
from django.core.cache import cache
import os
SITE_ROOT = os.path.dirname(os.path.realpath(__file__))
# Firs, atttempt in-python singletone
cloth_db_ = None
ranker_ = None

def cloth_db():
    global cloth_db_
    if cloth_db_ is not None:
        return cloth_db_
    #  cloth_db = cache.get('cloth_db')
    if cloth_db_ is None:
        cloth_db_ = core.cloth_db.ClothDB()
        cloth_db_.load_from_file(os.path.join(SITE_ROOT,"data/ItemsList.dat"))
    #  cache.set('cloth_db', cloth_db_)
    return cloth_db_

def ranker():
    global ranker_
    if ranker_ is not None:
        return ranker_
    # ranker_ = cache.get('ranker')
    if ranker_ is None:
        ranker_ = core.pair_ranker.RankerPairSimple()
        ranker_.load_from_file(os.path.join(SITE_ROOT,"training/pair_rating.dat"))
        ranker_.load_from_names_file(os.path.join(SITE_ROOT,"data/pair_ranks.txt"),
                                    lambda name: cloth_db().get_by_name(name).id)
    #cache.set('ranker', ranker_)
    return ranker_
