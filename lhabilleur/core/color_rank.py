import colorsys
import itertools
import struct


def hex_to_rgb(colors_hex):
    """Converts a list of hex colors to list of rgb triplets"""
    colors_rgb = map(lambda color: struct.unpack('BBB', color.decode('hex')),
                     colors_hex)
    return colors_rgb


def norm_rgb(x):
    """Input - list of components of rgb color"""
    return map(lambda color: float(color) / 255.0, x)


def unnorm_hsv(color_hsv):
    """Input - list of components of hsv color"""
    return [round(color_hsv[0] * 360), round(color_hsv[1] * 100), +\
            round(color_hsv[2] * 100)]


def rgb_to_hsv(colors_rgb):
    """Converts list of rgb colors to hsv colors"""
    colors_rgb_norm = map(norm_rgb, colors_rgb)
    colors_hsv = map(lambda color:
                     colorsys.rgb_to_hsv(*color), colors_rgb_norm)
    return map(unnorm_hsv, colors_hsv)

def hex_to_hsv(colors_hex):
    return rgb_to_hsv(hex_to_rgb(colors_hex))


def norm_hsv(x):
    return [float(x[0]) / 360, float(x[1]) / 100, +\
            float(x[2]) / 100]


def unnorm_rgb(x):
    return map(lambda color: round(color * 255.0), x)


def hsv_to_rgb(color_hsv):
    color_hsv_norm = norm_hsv(color_hsv)
    color_rgb = colorsys.hsv_to_rgb(*color_hsv_norm)
    return unnorm_rgb(color_rgb)


def rgb_to_hex(color_rgb):
    color_hex = '#' + struct.pack('BBB', *map(int, color_rgb)).encode('hex')
    return color_hex

def hsv_to_hex(color_hex):
    return rgb_to_hex(hsv_to_rgb(color_hex))

def color_rank(colors_hsv):
    """colors_hsv - list of triplets"""
    # Verification of input data format.
    if len(colors_hsv) == 0:
        return 0

    def is_incorrect_hsv(color):
        if not (0 <= color[0] <= 360) or not (0 <= color[1] <= 100) or not+\
               (0 <= color[2] <= 100):
            return True
        return False

    for color in colors_hsv:
        if (is_incorrect_hsv(color)):
            raise ValueError("Incorrect data")
            return 0

    # One color.
    if len(colors_hsv) == 1:
        return 3

    # Achromatic colors can be combined with any others.
    # Such colors are omitted.
    def is_chromatic(color_hsv):
        return (color_hsv[1] > 15 and color_hsv[2] > 15)
    chromatic_colors = filter(is_chromatic, colors_hsv)
    # We have less than 2 chromatic colors.
    # Rating is 3.
    if len(chromatic_colors) < 2:
        return 3

    if len(chromatic_colors) == 2:
        diff = abs(chromatic_colors[0][0] - chromatic_colors[1][0])
        diff = min(diff, 360 - diff)
        if not 11 <= diff <= 160:
            return 3
        if not 45 <= diff <= 125:
            return 2
        return 1

    # Input - pair of colors.
    def color_ditance(color_pair):
        diff = abs(color_pair[0][0] - color_pair[1][0])
        return min(diff, 360 - diff)

    # Calculates distances between colors.
    # Returns list of distances in ascending order.
    def calculate_distances(chromatic_colors):
        combinations = itertools.combinations(chromatic_colors, 2)
        distances = map(color_ditance, combinations)
        distances.sort()
        return distances

    if len(chromatic_colors) == 3:
        distance = calculate_distances(chromatic_colors)
        # Two similar colors and a complementary one.
        if distance[0] <= 20 and distance[1] >= 160:
            return 3

        if distance[0] <= 40 and distance[1] >= 140:
            return 2

        # Three similar colors.
        if distance[0] <= 10 and distance[1] <= 20 and+\
           distance[0] + distance[1] == distance[2]:
            return 3

        if distance[0] <= 20 and distance[1] <= 40 and+\
           distance[0] + distance[1] == distance[2]:
            return 2

        return 1

    if len(chromatic_colors) == 4:
        distance = calculate_distances(chromatic_colors)

        # Three similar colors and complementary one.
        if distance[0] <= 10 and distance[1] <= 10 and+\
           distance[0] + distance[1] == distance[2] and+\
           distance[3] >= 155:
            return 3

        if distance[0] <= 20 and distance[1] <= 20 and+\
           distance[0] + distance[1] == distance[2] and+\
           distance[3] >= 135:
            return 2

        # Four similar colors.
        if distance[0] <= 10 and distance[1] <= 10 and distance[2] <= 10 and+\
           distance[0] + distance[1] + distance[2] == distance[5]:
            return 3

        if distance[0] <= 20 and distance[1] <= 20 and distance[2] <= 20 and+\
           distance[0] + distance[1] + distance[2] == distance[5]:
            return 2

        # Two pairs of similar colors.
        if distance[0] <= 10 and distance[1] <= 10 and+\
           distance[2] >= 155:
            return 3

        if distance[0] <= 20 and distance[1] <= 20 and+\
           distance[2] >= 135:
            return 2

        return 1

    raise ValueError("Incorrect data")


if __name__ == '__main__':
    test_data = ["8C00FF", "6500FF"]
    test_res = [(140, 0, 255), (101, 0, 255)]
    good = (hex_to_rgb(test_data) == test_res)
    print("hex_to_rgb", good)

    # input - rgb hex string
    test_set = zip([["000000"],
                    ["515DAD", "F9A82F"],
                    ["8C00FF", "6500FF", "3F00FF"],
                    ["8C00FF", "6500FF", "3F00FF", "0000FF"],
                    ["8C00FF", "6500FF", "3F00FF", "AAAAAA"]],
                   [3, 2, 3, 2, 1])
    good = map(lambda (colorset, val):
               color_rank(rgb_to_hsv(hex_to_rgb(colorset))) == val, test_set)
    print(good)
    print(color_rank([[64.0, 16.0, 72.0], [71.0, 13.0, 59.0],
                      [201.0, 46.0, 91.0]]))
