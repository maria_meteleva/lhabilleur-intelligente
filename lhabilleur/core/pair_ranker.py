import color_rank
import itertools

class RankerPairSimple():
    def __init__(self):
        """Ranker. Rank is the minimum of all the pair ranks"""
        self.rankings = dict()
        self.rank_colors = color_rank.color_rank

    def load_from_file(self, filename):
        ranking_file = open(filename)
        for line in ranking_file:
            line = line.strip()
            if not line[0] == "#":
                parsed = map(int, line.split(' '))
                ids = frozenset({parsed[1], parsed[3]})
                self.rankings[ids] = parsed[4]
        ranking_file.close()

    def load_from_names_file(self, filename, id_from_name):
        """
        Initialzes from a following file:
        name1 name2 rank
        id_from_nmae - function that maps names to ids
        """
        ranking_file = open(filename)
        for line in ranking_file:
            line = line.strip(' \n')
            if not line[0] == "#":
                line_spl = unicode(line,
                                   encoding='utf-8').strip(
                    ' \n' + unichr(65279)).split()
                names = line_spl[0:2]
                try:
                    ids = frozenset(map(id_from_name, names))
                    self.rankings[ids] = int(line_spl[2])
                except KeyError:
                    print "Error initilising", names[0], names[1]
                    import pydb
                    pydb.set_trace()
        ranking_file.close()

    def rank_items(self, ids):
        # Recursive workhorse of ranking. ids is a list of ids
        if len(ids) == 1:
            return 3
        else:
            rank = min(map(lambda items: self.rankings[frozenset(items)],
                           itertools.combinations(ids, 2)))
            return rank

    def rank(self, suit):
        """Suit - list of Cloth"""
#        cloth_db_ = cloth_db.ClothDB()
        ids = map(lambda item: item.id, suit)
        colors = map(lambda item: item.color, suit)
        # TODO(kazeevn) do we need the verification here?
#        if not cloth_db_.check_suit(ids):
#            raise Exception("Invlid suite!")
        return min(self.rank_items(ids), self.rank_colors(colors))

    def fit_rank(self, suit, suit_rank, item):
        colors = map(lambda item: item.color, suit)
        ids = map(lambda item: item.id, suit)
        color_rank_ = self.rank_colors(colors + [item.color, ])
        item_rank = min(
            map(lambda suit_item: self.rankings[
                frozenset({item.id, suit_item})], ids))
        the_rank = min(color_rank_, suit_rank, item_rank)
        # If we can match at least something, we should
        if color_rank_ != item_rank:
            the_rank += 0.1
        return the_rank

    def fill_slot(self, suit, items_list, num=1):
        """Returns num. items from items_list and their rankings. suit
        and item_list - lists of Cloth"""
        ids = map(lambda item: item.id, suit)
        suit_rank = self.rank_items(ids)
        ranks = map(lambda item: self.fit_rank(suit, suit_rank, item),
                    items_list)
        the_list = zip(items_list, ranks)
        the_list.sort(key=lambda item: item[1], reverse=True)
        return the_list[-num:]
