import collections

PlainCloth = collections.namedtuple("PlainCloth", "id, slot, name")
Cloth = collections.namedtuple("Cloth", "id, color")
# plain - as it doesn't hold color

def assemble_suit(ids, colors):
    """For converting old tests, which didn't use Cloth"""
    return map(lambda item: Cloth(*item), zip(ids, colors))


def assemble_grey_suit(ids):
    """Assembles a colorless suit"""
    return map(lambda item: Cloth(item, (0, 0, 0)), ids)

class ClothDB:
    """Database of all clothes."""
    def __init__(self):
        self.clothes_by_id = dict()
        self.clothes_by_slot = dict()
        self.clothes_by_name = dict()
        self.slot_ids = dict()
        self.slot_by_id = dict()
        self.slots = self.clothes_by_slot.keys
        self.ids = self.clothes_by_id.keys
        self.names = self.clothes_by_name.keys
        self.get_by_id = self.clothes_by_id.__getitem__
        self.get_by_slot = self.clothes_by_slot.__getitem__
        self.get_by_name = self.clothes_by_name.__getitem__
        self.__len__ = self.clothes_by_id.__len__

    def __repr__(self):
        """Nice, user friendly representation"""
        return str(self.clothes_by_name.keys())

    def add(self, cloth):
        self.clothes_by_id[cloth.id] = cloth
        self.clothes_by_name[cloth.name] = cloth
        # If names are not unique, you are skrewedb
        if not cloth.slot in self.clothes_by_slot.keys():
            self.clothes_by_slot[cloth.slot] = list()

        self.get_by_slot(cloth.slot).append(self.clothes_by_id[cloth.id])

    def load_from_file(self, filename):
        # TODO(kazeevn) watch for multiple initializations
        clothes_file = open(filename)
        for line in clothes_file:
            line = line.strip()
            if not line[0] == "#":
                params = line.split('\t')
                self.add(PlainCloth(int(params[1]),
                                    unicode(params[0], encoding='utf-8'),
                                    unicode(params[2], encoding='utf-8')))
        clothes_file.close()
        i = 1
        for slot in self.slots():
            self.slot_by_id[i] = slot
            self.slot_ids[slot] = i
            i += 1

    def check_suit(self, suit):
        """Returns if all slots are different and ids are present in the db"""
        for item in suit:
            if item not in self.ids():
                return False
        slots = map(lambda item: self.get_by_id(item).slot, suit)
        return (len(slots) == len(set(slots)))
