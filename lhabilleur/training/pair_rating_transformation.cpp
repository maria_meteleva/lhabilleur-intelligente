#include <fstream>
#include <string>
#include <vector>

using std::vector;
using std::string;

void ConvertToElements(string rating_file, string items_file,
                       int number_of_pairs, int number_of_items) {
  std::ifstream ifs_rating(rating_file.c_str(), std::ios::in);
  std::ifstream ifs_items(items_file.c_str(), std::ios::in);

  vector<string> clothes;
  int slot;
  int num_of_element;
  string element;

  for (int i = 0; i < number_of_items; ++i) {
    ifs_items >> slot >> num_of_element >> element;
    clothes.push_back(element);
  }

  std::ofstream ofs("result.txt", std::ios::out);
  int rating;

  // Items in the clothes file must be arranged so
  // that line number is equal to id.
  for (int i = 0; i < number_of_pairs; ++i) {
    for (int j = 0; j < 2; ++j) {
      ifs_rating >> slot >> num_of_element;
      ofs << clothes[num_of_element - 1] << ' ';
    }
    ifs_rating >> rating;
    ofs << rating << std::endl;
  }
}

int main() {
  ConvertToElements("pair_rating.dat", "ItemsList.dat", 121, 18);
  return 0;
}
