import sys
import time
import random
import numpy as np
from core import *

# The script accepts following filenames
# input: Clothes list in form (slot, id, name)
# output: suits list in form (slot1, id1, ... , sloti, idi rating)
# pair_ranks: if present, 2-piece suits are not asked about

# Since test subjects will quickly become bored with the task
# here is my take on GlaDos humor)
# jokes = (
#     "Pointless task, isn't it?",
#     "Now you will have to start from the beginning.",
#     "Are you serious anyone will ever use this?",
#     "Your dress is terrefying. In the literal meaning.",
#     "Maybe such work should be done by robots?",
#     "Excuse me, but would you stop staring at me?",
# )
# compliments = (
#
# OK, may be not funny
jokes = ['',]

errors = 0
def ask(suit):
    global errors
    rating = -1
    repeat = False
    while rating < 0 or rating > 3:
        if repeat:
            print("Mistake in input. BTW " + random.choice(jokes))
            errors += 1
            time.sleep(2)
            print("Thank you. Now we can continue.")
        try:
            rating = int(raw_input(reduce(lambda line, item: line+" "+
                                          wardrobe.get_by_id(item).name,
                                          suit,"Rank 0-3: ")+" = "))
        except ValueError:
            rating = -1
        repeat = True
    return rating


wardrobe = pair_ranker.Wardrobe()
wardrobe.load_from_file(sys.argv[1])
outfile = open(sys.argv[2], 'w')
our_pair_ranker = None
try:
    our_pair_ranker = pair_ranker.RankerPairSimple(sys.argv[3])
    print("Pair ranks successfully loaded from %s"%sys.argv[3])
except:
    print("Failed to establish pair rankings.")

gen_suits = suit_generator.SuitGenerator(wardrobe)
max_id = max(wardrobe.ids())
N = 0
for suit in gen_suits:
    if len(suit) == 2 and not our_pair_ranker is None:
        rating = our_pair_ranker.rank(suit)
    else:
        rating = ask(suit)
    suit = str(pair_ranker.bag_of_words(suit, max_id)).strip("[] ")
    outfile.write(suit+" %d\n"%rating)
    N += 1
print("Processed %d suits, made %d mistakes." % (N, errors))

outfile.close()
