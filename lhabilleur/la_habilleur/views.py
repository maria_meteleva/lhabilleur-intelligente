from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest
from django.shortcuts import get_object_or_404, render
from django.utils.datastructures import MultiValueDictKeyError
from django.utils import simplejson

import init_and_cache
import core
from la_habilleur.models import Wardrobe
from core.color_rank import hsv_to_hex

def index(request):
    return HttpResponse("Hello")

def display_wardrobe(request):
    the_wardrobe = get_object_or_404(Wardrobe, pk=1)
    return render(request, 'la_habilleur/add_cloth.html', {
        'the_wardrobe': the_wardrobe.wardrobe,
        'cloth_db':  init_and_cache.cloth_db()})

def add_to_wardrobe(request):
    the_wardrobe = get_object_or_404(Wardrobe, pk=1)
    try:
        id_to_add = init_and_cache.cloth_db().get_by_name(
            request.POST['cloth_name']).id
    except (ValueError, MultiValueDictKeyError, KeyError):
        return render(request, 'la_habilleur/add_cloth.html', {
         'the_wardrobe': the_wardrobe.wardrobe,
            'cloth_db': init_and_cache.cloth_db(),
            "error_message": "Invalid item"})
    try:
        color_to_add = tuple(core.color_rank.hex_to_hsv(
            [request.POST['cloth_color'].strip('#'),])[0])
    except (ValueError, MultiValueDictKeyError, KeyError, TypeError):
        return render(request, 'la_habilleur/add_cloth.html', {
            'the_wardrobe': the_wardrobe.wardrobe,
            'cloth_db': init_and_cache.cloth_db(),
            "error_message": "Invalid color"})

    cloth_to_add = core.cloth_db.Cloth(id_to_add, color_to_add)
    if not isinstance(the_wardrobe.wardrobe, list):
        the_wardrobe.wardrobe = list()
    if cloth_to_add in the_wardrobe.wardrobe:
        return render(request, 'la_habilleur/add_cloth.html', {
            'the_wardrobe': the_wardrobe.wardrobe,
            'cloth_db': init_and_cache.cloth_db(),
            "error_message": "Duplicate entry"})
    the_wardrobe.wardrobe.append(cloth_to_add)
    the_wardrobe.save()
    return HttpResponseRedirect(reverse('la_habilleur:display-wardrobe'))

def drop_wardrobe(request):
    the_wardrobe = get_object_or_404(Wardrobe, pk=1)
    the_wardrobe.wardrobe = []
    the_wardrobe.save()
    return HttpResponseRedirect(reverse('la_habilleur:display-wardrobe'))

def drop_suits(request):
    the_wardrobe = get_object_or_404(Wardrobe, pk=1)
    the_wardrobe.suits = []
    the_wardrobe.save()
    return HttpResponseRedirect(reverse('la_habilleur:manage-suits'))

def enter_suit(request):
    return render(request, 'la_habilleur/calc_suit_rank.html', {})

def view_suit_rank(request):
    cloth_ids = request.POST['suit']
    cloth_ids = cloth_ids.split()
    cloth_ids = map(int, cloth_ids)
    ranker = init_and_cache.ranker()
    rank = ranker.rank_items(cloth_ids)
    return HttpResponse(rank)

def add_suit(request):
    """Simple debug function for adding suits"""
    the_wardrobe = get_object_or_404(Wardrobe, pk=1)
    cloth_ids = request.POST['suit']
    cloth_ids = cloth_ids.split()
    cloth_ids = tuple(map(int, cloth_ids))
    if len(cloth_ids) == 0:
          return HttpResponseBadRequest(
            "Can't enter an empty suit")
    if not init_and_cache.cloth_db().check_suit(cloth_ids):
        return HttpResponseBadRequest(
            "This is not a suit. Some ids belong to the same slot")
    if not isinstance(the_wardrobe.suits, list):
        the_wardrobe.suits = list()
    the_wardrobe.suits.append(core.cloth_db.assemble_grey_suit(cloth_ids))
    the_wardrobe.save()
    return HttpResponse("Added")

def manage_suits(request, suit_id):
    the_wardrobe = get_object_or_404(Wardrobe, pk=1)
    try:
        suit = the_wardrobe.suits[int(suit_id)]
    except ValueError:
        suit = []
    except IndexError:
        return HttpResponseBadRequest("No suit with such id")

    return render(request, 'la_habilleur/manage_suits.html', {
        'the_wardrobe': the_wardrobe.wardrobe,
        'cloth_db':  init_and_cache.cloth_db(),
        'suit': suit,    # Suit being displayed
        'suits': the_wardrobe.suits   # All suits of the user
        })

def get_wardrobe(request):
    """Returns JSON with the user wardrobe. Format:
    1. json of cloth
    2. Each cloth: json of
    a. id - python cloth_db id
    b. name - python cloth_db name
    c. color - hex color
    d. slot - slot id, as in slot iterator
    """
    the_wardrobe = get_object_or_404(Wardrobe, pk=1)
    cloth_db_ = init_and_cache.cloth_db()
    response = list()
    for cloth in the_wardrobe.wardrobe:
        cloth_dict = dict()
        cloth_in_db = cloth_db_.get_by_id(cloth.id)
        cloth_dict['color'] = hsv_to_hex(cloth.color)
        cloth_dict['name'] = cloth_in_db.name
        cloth_dict['slot'] = cloth_db_.slot_ids[cloth_in_db.slot]
        cloth_dict['id'] = cloth.id
        response.append(cloth_dict)

    return HttpResponse(
        simplejson.dumps(response, ensure_ascii=False),
        content_type = 'application/json; charset=utf8'
    )
