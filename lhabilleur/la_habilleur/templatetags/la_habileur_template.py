import colorsys
import struct
from django import template
from django.utils.safestring import mark_safe
from itertools import ifilter

import init_and_cache
register = template.Library()


@register.filter
def lookup(d, key):
    return d[key]

@register.filter
def clothes_from_wardrobe_by_slot(wardrobe, slot):
    cloth_db_ = init_and_cache.cloth_db()
    return filter(lambda cloth: cloth_db_.get_by_id(cloth.id).slot ==
                  slot, wardrobe)

@register.filter()
def index(list, item):
    return list.index(item)

def norm_hsv(x):
    return [float(x[0]) / 360, float(x[1]) / 100, +\
            float(x[2]) / 100]


def unnorm_rgb(x):
    return map(lambda color: round(color * 255.0), x)


def hsv_to_rgb(color_hsv):
    color_hsv_norm = norm_hsv(color_hsv)
    color_rgb = colorsys.hsv_to_rgb(*color_hsv_norm)
    return unnorm_rgb(color_rgb)


def rgb_to_hex(color_rgb):
    color_hex = '#' + struct.pack('BBB', *map(int, color_rgb)).encode('hex')
    return color_hex


@register.filter
def hsv_to_hex(color_hex):
    return rgb_to_hex(hsv_to_rgb(color_hex))


@register.filter
def print_element(cloth_db_by_id, key):
    return cloth_db_by_id[key].name.encode('utf-8')

@register.filter(needs_autoescape=False)
def print_suit(suit):
    """Nicely prints suit"""
    return mark_safe(', '.join(map(color_background_cloth, suit)))

@register.filter(needs_autoescape=False)
def color_background_cloth(cloth):
    """Returns cloth name on a html-colored background. Color is in HSV"""
    cloth_db_ = init_and_cache.cloth_db()
    if cloth.color[2] > 20:  # Background is not going to be black
        return mark_safe('<span style="color: #000000; background-color: %s">%s</span>' % (
            hsv_to_hex(cloth.color), cloth_db_.get_by_id(cloth.id).name))
    else:
        return  mark_safe('<span style="color: #FFFFFF; background-color: %s">%s</span>' % (
            hsv_to_hex(cloth.color), cloth_db_.get_by_id(cloth.id).name))

@register.filter()
def print_find_by_slot(suit, slot):
    """Returns the colored cloth from suit, that matches the slot. Or None"""
    cloth_db_ = init_and_cache.cloth_db()
    the_cloth = next(ifilter(lambda cloth: cloth_db_.get_by_id(cloth.id).slot == slot,
                             suit), None)
    if the_cloth is None:
        return ""
    else:
        return color_background_cloth(the_cloth)

