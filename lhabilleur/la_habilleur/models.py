from django.db import models
from picklefield.fields import PickledObjectField


class Wardrobe(models.Model):
    username = models.CharField(max_length = 100)
    wardrobe = PickledObjectField()
    suits = PickledObjectField()

    def __unicode__(self):
        return self.username

    def best_suits(self):
        return None

    def add_cloth(self):
        return None

    def fill_the_suit(self):
        return None
