// Copyright 2013 Nikita Kazeev, Meteleva Maria
// Suit convention
// 1. json of cloth
// 2. Each cloth: json of
// a. id - python cloth_db id
// b. name - python cloth_db name
// c. color - hex color
// d. slot - slot id, as in slot iterator

var wardrobe = {};
var suit = {};
var max_slot = 0;

function UpdateSelection(cloth_name) {
    $("#cloth_to_add").val(cloth_name);
}

function UpdateSelectionAndColor(cloth_name) {
    $("#cloth_to_add").val(cloth_name);
}

function DisplaySuit() {
    // Displays current suit on webpage as selected
    for (var slot = 1; slot <= max_slot; slot++) {
	var item = suit[slot];
	var label = "";
	if (typeof(item) != "undefined")
	    label = NicePrint(item)
	$("#dressed_slot_"+slot).html(label);
    }
}

function NicePrint(cloth) {
    var color_value  = tinycolor(cloth["color"]).toHsv()["v"];
    if (color_value > 20.0/255.0)
	return '<span style="color: #000000; background-color: ' + cloth["color"] +
	    '">'+cloth.name+'</span>';
    else
	return '<span style="color: #FFFFFF; background-color: ' + cloth["color"] +
	    '">'+cloth.name+'</span>';
}

function AddToCurrentSuit(wd_id) {
    // Adds the selected item to the suit
    // TODO(kazeevn) it's acually a stub
    suit[ wardrobe[wd_id]["slot"]] = wardrobe[wd_id]
    DisplaySuit();
}

$('document').ready(function() {
    var wardrobe_request = $.getJSON(
	"/la_habilleur/get_wardrobe/").done(function(data) {
	    wardrobe = data;
	    for (item in wardrobe) {
		if (max_slot < wardrobe[item]["slot"])
		    max_slot = wardrobe[item]["slot"];
	    }
	    suit.length = max_slot + 1;
	});
    $("#cloth_color_selector").colorpicker();
})