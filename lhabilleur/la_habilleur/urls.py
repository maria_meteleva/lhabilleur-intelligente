from django.conf.urls.defaults import patterns, url
from la_habilleur import views

urlpatterns = patterns(
    '',
    url(r'^$', views.display_wardrobe, name='display-wardrobe'),
    url(r'^enter-suit/', views.enter_suit, name = 'enter-suit'),
    url(r'^view-suit-rank/', views.view_suit_rank, name = 'view-suit-rank'),
    url(r'^add-to-wardrobe/', views.add_to_wardrobe, name='add-to-wardrobe'),
    url(r'^drop-wardrobe/', views.drop_wardrobe, name='drop-wardrobe'),
    url(r'^drop-suits/', views.drop_suits, name='drop-suits'),
    url(r'^manage-suits/(?P<suit_id>\d*)/?', views.manage_suits,
        name='manage-suits'),
    url(r'^add-suit/', views.add_suit, name='add-suit'),
    # Convention: json-giving urls include _
    url(r'^get_wardrobe/', views.get_wardrobe, name='get_wardrobe')
)
