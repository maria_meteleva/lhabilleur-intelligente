from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
from django.contrib import admin
from django.conf import settings
from django.http import HttpResponseRedirect
admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
#    (r'^static/(?P<path>.*)$', 'django.views.static.serve',
#     {'document_root': settings.STATIC_ROOT}),
    url(r'^la_habilleur/', include('la_habilleur.urls',
                                   namespace='la_habilleur')),
     url(r'^$', lambda x: HttpResponseRedirect("la_habilleur/"))
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
)

